﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Web.Script.Serialization;

namespace TouchConsoleStudio
{
    public enum ctlType
    {
        page,
        label,
        button
    }

    public enum ctlBorder
    {
        none,
        solid,
        strong
    }

    public enum ctlBackImageStyle
    {
        normal,
        center,
        stretch,
        multiply
    }

    public abstract class control
    {
        [CategoryAttribute("Global"),
        ReadOnlyAttribute(true),
        DescriptionAttribute("Types: page, label, button")]
        public ctlType type { set; get; }

        [CategoryAttribute("Global"),
        ReadOnlyAttribute(true),
        DescriptionAttribute("Unique identification number")]
        public int id { set; get; }

        [ScriptIgnore]
        [CategoryAttribute("Global")]
        public string name { set; get; }


        [BrowsableAttribute(false)]
        public int[] location = new int[2];
        [ScriptIgnore]
        [CategoryAttribute("Layout "),
        DisplayName("location")]
        public Point location_
        {
            set { location[0] = value.X; location[1] = value.Y; }
            get { return new Point(location[0], location[1]); }
        }

        [CategoryAttribute("Appearance "),
        DescriptionAttribute("Borders: none, solid, strong")]
        public ctlBorder border { set; get; }

        [BrowsableAttribute(false)]
        public int[] backColor = new int[4];
        [ScriptIgnore]
        [CategoryAttribute("Appearance "),
        DisplayName("backColor")]
        public Color backColor_
        {
            get { return Color.FromArgb(backColor[3], backColor[0], backColor[1], backColor[2]); }
            set { backColor[0] = value.R; backColor[1] = value.G; backColor[2] = value.B; backColor[3] = value.A; }
        }

        [BrowsableAttribute(false)]
        public int[] borderColor = new int[4];
        [ScriptIgnore]
        [CategoryAttribute("Appearance "),
        DisplayName("borderColor")]
        public Color borderColor_
        {
            get { return Color.FromArgb(borderColor[3], borderColor[0], borderColor[1], borderColor[2]); }
            set { borderColor[0] = value.R; borderColor[1] = value.G; borderColor[2] = value.B; borderColor[3] = value.A; }
        }

        [CategoryAttribute("Appearance ")]
        public string backImage { set; get; }

        [CategoryAttribute("Appearance ")]
        public ctlBackImageStyle backImageStyle { set; get; }

        [BrowsableAttribute(false)]
        public int[] backImageOffset = new int[2];
        [ScriptIgnore]
        [CategoryAttribute("Appearance "),
        DisplayName("backImageOffset")]
        public Point backImageOffset_
        {
            set { backImageOffset[0] = value.X; backImageOffset[1] = value.Y; }
            get { return new Point(backImageOffset[0], backImageOffset[1]); }
        }
    }
}
