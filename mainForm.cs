﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpGL;
using System.Web.Script.Serialization;

namespace TouchConsoleStudio
{
    public partial class mainForm : Form
    {
        private void label_Paint(object sender, PaintEventArgs e)
        {
            Label l = sender as Label;
            int left = TextRenderer.MeasureText(l.Text, l.Font).Width + 3;
            base.OnPaint(e);
            e.Graphics.DrawLine(new Pen(SystemColors.ControlDark), left, l.Height / 2, left + (l.Width - left - 5), l.Height / 2);
        }
        // majd egyszer...

        ctrlPage currentPage = new ctrlPage();
        control currentControl;

        public mainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            currentPage.name = "mainPage";
            currentPage.controls.Add(new ctrlLabel());
            currentPage.controls.Last().name = "label1";

            TreeNode no = new TreeNode(currentPage.name);
            no.Tag = currentPage;
            projectTV.Nodes[0].Nodes.Add(no);
            

            //loadPage(currentPage);
        }

        private void designerGL_OpenGLInitialized(object sender, EventArgs e)
        {
            OpenGL gl = designerGL.OpenGL;
            gl.ClearColor(0, 0, 0, 0);
        }

        private void designerGL_Resized(object sender, EventArgs e)
        {
            OpenGL gl = designerGL.OpenGL;
            gl.MatrixMode(OpenGL.GL_PROJECTION);
            gl.LoadIdentity();
            gl.Ortho2D(0, (double)designerGL.Width, (double)designerGL.Height, 0);
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
        }

        private void designerGL_OpenGLDraw(object sender, RenderEventArgs args)
        {
            OpenGL gl = designerGL.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT);
            gl.LoadIdentity();

            gl.DrawText(10, designerGL.Height - 20, 1, 1, 1, "Courier New", 12.0f, "Teszt");

            /*gl.Begin(OpenGL.GL_QUADS);               // Each set of 4 vertices form a quad
            gl.Color(1.0f, 0.0f, 0.0f);  // Red
            gl.Vertex(-50.0f, -50.0f);     // Define vertices in counter-clockwise (CCW) order
            gl.Vertex(50.0f, -50.0f);     //  so that the normal (front-face) is facing you
            gl.Vertex(50.0f, 50.0f);
            gl.Vertex(-50.0f, 50.0f);
            gl.End();*/
        }

        public void loadSource(control inC)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            sourceS.Text = "$" + inC.name + " = " + new JsonFormatter(ser.Serialize(inC) + ";", true).Format();
        }

        public void loadPage(ctrlPage inP)
        {
            tabPage1.Text = "Designer - " + inP.name;
            tabPage2.Text = "Source - " + inP.name;
            loadSource(inP);
            currentPage = inP;
            propertiesG.SelectedObject = inP;

            controlsLV.VirtualListSize = inP.controls.Count;
            controlsLV.Refresh();
        }

        public void setCurrentControl(control inC)
        {
            tabPage1.Text = "Designer - " + inC.name;
            tabPage2.Text = "Source - " + inC.name;
            loadSource(inC);
            currentControl = inC;
            propertiesG.SelectedObject = inC;
        }

        private void propertiesG_PropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
        {
            loadSource((sender as PropertyGrid).SelectedObject as control);
        }

        private void controlsLV_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            ListViewItem ret = new ListViewItem();

            ret.Text = currentPage.controls[e.ItemIndex].name;
            ret.SubItems.Add(Enum.GetName(typeof(ctlType), currentPage.controls[e.ItemIndex].type));

            e.Item = ret;
        }

        private void controlsLV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (controlsLV.SelectedIndices.Count != 1) return;
            setCurrentControl(currentPage.controls[controlsLV.SelectedIndices[0]]);
        }

        private void projectTV_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if ((e.Node.Tag != null) && (e.Node.Tag is ctrlPage))
            {
                loadPage(e.Node.Tag as ctrlPage);
            }
        }

        private void projectTV_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            projectTV_AfterSelect(sender, new TreeViewEventArgs(e.Node));
        }

        private void controlsLV_MouseClick(object sender, MouseEventArgs e)
        {
            controlsLV_SelectedIndexChanged(sender, null);
        }

        

    }
}
