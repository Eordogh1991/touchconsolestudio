﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Drawing;

namespace TouchConsoleStudio
{
    public class ctrlLabel : control
    {
        [CategoryAttribute("Appearance ")]
        public string font { set; get; }

        [CategoryAttribute("Appearance ")]
        public string text { set; get; }

        [BrowsableAttribute(false)]
        public int[] textColor = new int[4];
        [ScriptIgnore]
        [CategoryAttribute("Appearance "),
        DisplayName("textColor")]
        public Color textColor_
        {
            get { return Color.FromArgb(textColor[3], textColor[0], textColor[1], textColor[2]); }
            set { textColor[0] = value.R; textColor[1] = value.G; textColor[2] = value.B; textColor[3] = value.A; }
        }

        [CategoryAttribute("Appearance ")]
        public bool visible { set; get; }


        public ctrlLabel()
        {
            type = ctlType.label;
        }
    }
}
