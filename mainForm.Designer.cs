﻿namespace TouchConsoleStudio
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Pages");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Sources");
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.designerGL = new SharpGL.OpenGLControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.sourceS = new ScintillaNET.Scintilla();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.propertiesG = new System.Windows.Forms.PropertyGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.controlsLV = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.projectTV = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.designerGL)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceS)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1201, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statusStrip1.Location = new System.Drawing.Point(0, 767);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1201, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1201, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.splitter2);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 49);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.panel2.Size = new System.Drawing.Size(1201, 718);
            this.panel2.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(199, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(747, 713);
            this.panel4.TabIndex = 9;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tabControl1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel7.Size = new System.Drawing.Size(747, 713);
            this.panel7.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(68, 20);
            this.tabControl1.Location = new System.Drawing.Point(5, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(737, 708);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.designerGL);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(729, 680);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Designer";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // designerGL
            // 
            this.designerGL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.designerGL.DrawFPS = false;
            this.designerGL.Location = new System.Drawing.Point(3, 3);
            this.designerGL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.designerGL.Name = "designerGL";
            this.designerGL.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.designerGL.RenderContextType = SharpGL.RenderContextType.DIBSection;
            this.designerGL.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.designerGL.Size = new System.Drawing.Size(723, 674);
            this.designerGL.TabIndex = 0;
            this.designerGL.OpenGLInitialized += new System.EventHandler(this.designerGL_OpenGLInitialized);
            this.designerGL.OpenGLDraw += new SharpGL.RenderEventHandler(this.designerGL_OpenGLDraw);
            this.designerGL.Resized += new System.EventHandler(this.designerGL_Resized);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.sourceS);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(729, 680);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Source";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // sourceS
            // 
            this.sourceS.ConfigurationManager.Language = "js";
            this.sourceS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceS.Indentation.TabWidth = 4;
            this.sourceS.Location = new System.Drawing.Point(3, 3);
            this.sourceS.Margins.Margin0.Width = 30;
            this.sourceS.Name = "sourceS";
            this.sourceS.Size = new System.Drawing.Size(723, 674);
            this.sourceS.Styles.BraceBad.Size = 9F;
            this.sourceS.Styles.BraceLight.Size = 9F;
            this.sourceS.Styles.CallTip.FontName = "Segoe U";
            this.sourceS.Styles.ControlChar.Size = 9F;
            this.sourceS.Styles.Default.BackColor = System.Drawing.SystemColors.Window;
            this.sourceS.Styles.Default.Size = 9F;
            this.sourceS.Styles.IndentGuide.Size = 9F;
            this.sourceS.Styles.LastPredefined.Size = 9F;
            this.sourceS.Styles.LineNumber.Size = 9F;
            this.sourceS.Styles.Max.Size = 9F;
            this.sourceS.TabIndex = 0;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(946, 5);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(5, 713);
            this.splitter2.TabIndex = 8;
            this.splitter2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.splitter3);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(951, 5);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.panel3.Size = new System.Drawing.Size(250, 713);
            this.panel3.TabIndex = 7;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.propertiesG);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 227);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(245, 481);
            this.panel6.TabIndex = 2;
            // 
            // propertiesG
            // 
            this.propertiesG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertiesG.Location = new System.Drawing.Point(0, 25);
            this.propertiesG.Name = "propertiesG";
            this.propertiesG.Size = new System.Drawing.Size(245, 456);
            this.propertiesG.TabIndex = 2;
            this.propertiesG.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertiesG_PropertyValueChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(245, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Properties";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Paint += new System.Windows.Forms.PaintEventHandler(this.label_Paint);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 222);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(245, 5);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.controlsLV);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(245, 222);
            this.panel5.TabIndex = 0;
            // 
            // controlsLV
            // 
            this.controlsLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.controlsLV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlsLV.FullRowSelect = true;
            this.controlsLV.GridLines = true;
            this.controlsLV.HideSelection = false;
            this.controlsLV.Location = new System.Drawing.Point(0, 25);
            this.controlsLV.MultiSelect = false;
            this.controlsLV.Name = "controlsLV";
            this.controlsLV.Size = new System.Drawing.Size(245, 197);
            this.controlsLV.TabIndex = 2;
            this.controlsLV.UseCompatibleStateImageBehavior = false;
            this.controlsLV.View = System.Windows.Forms.View.Details;
            this.controlsLV.VirtualMode = true;
            this.controlsLV.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.controlsLV_RetrieveVirtualItem);
            this.controlsLV.SelectedIndexChanged += new System.EventHandler(this.controlsLV_SelectedIndexChanged);
            this.controlsLV.MouseClick += new System.Windows.Forms.MouseEventHandler(this.controlsLV_MouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Type";
            this.columnHeader2.Width = 70;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Controls";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Paint += new System.Windows.Forms.PaintEventHandler(this.label_Paint);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.splitter1.Location = new System.Drawing.Point(194, 5);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 713);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.projectTV);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel1.Size = new System.Drawing.Size(194, 713);
            this.panel1.TabIndex = 5;
            // 
            // projectTV
            // 
            this.projectTV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectTV.FullRowSelect = true;
            this.projectTV.HideSelection = false;
            this.projectTV.ItemHeight = 20;
            this.projectTV.Location = new System.Drawing.Point(5, 25);
            this.projectTV.Name = "projectTV";
            treeNode3.Name = "nodePages";
            treeNode3.Text = "Pages";
            treeNode4.Name = "nodeSources";
            treeNode4.Text = "Sources";
            this.projectTV.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4});
            this.projectTV.ShowLines = false;
            this.projectTV.Size = new System.Drawing.Size(184, 683);
            this.projectTV.TabIndex = 1;
            this.projectTV.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.projectTV_AfterSelect);
            this.projectTV.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.projectTV_NodeMouseClick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project explorer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Paint += new System.Windows.Forms.PaintEventHandler(this.label_Paint);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 789);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Touch Console Studio - vALPHA 0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.designerGL)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sourceS)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView projectTV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PropertyGrid propertiesG;
        private SharpGL.OpenGLControl designerGL;
        private ScintillaNET.Scintilla sourceS;
        private System.Windows.Forms.ListView controlsLV;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}

